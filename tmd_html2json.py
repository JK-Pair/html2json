import glob
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from unicodedata import normalize
import calendar
import argparse
import json
# ROOT_DIR = os.path.abspath(os.curdir)

class tmd_html2json:
    def __init__(self, csv_file_name):
        '''
        csv_file_name [str]: csv file path
        '''
        self.csv_file_name = csv_file_name
        self.start_date_pos: int = 3
        self.end_date_pos: int = -1
        self.list_global: list = list()
        self.dict_dataframe = dict()

    def read_html(self, weather_file):
        try:
            os.path.isfile(weather_file)
        except:
            print("There is no {} file".format(weather_file))

        df_table = pd.read_html(weather_file, na_values=("-"))[0]
        head_table = df_table.iloc[1]
        body_table = df_table.iloc[2:]
        return head_table, body_table.reset_index(drop=True)

    def get_station_id(self, metadata_path):
        csv_read = pd.read_csv(metadata_path)
        station_id = dict(zip(list(csv_read.tmd_code), list(csv_read.wmo_code)))
        return station_id

    def reformat_every_day(self, head_table, body_table):
        dd_values = head_table.iloc[self.start_date_pos: self.end_date_pos].to_list()
        dd_index = head_table.iloc[self.start_date_pos: self.end_date_pos].index.to_list()

        location_col = body_table.iloc[:, 1].to_list()
        mm_yyyy_col = body_table.iloc[:, 2].to_list()

        dict_dataframe = dict()
        for seq in range(len(mm_yyyy_col)):
            temp_dict_in = dict()
            index = body_table[(body_table[2] == mm_yyyy_col[seq]) & (body_table[1] == location_col[seq])].index.values[0]
            tmd_station = int(self.station_id[int(location_col[seq].split("-")[0])])
            split_year_month = mm_yyyy_col[seq].split("/")

            for date in range(len(dd_values)):
                date_arranged = split_year_month[-1] + "-" + '%02d' % int(split_year_month[0]) + "-" + '%02d' % int(dd_values[date])
                get_values_by_date = body_table.iloc[index, int(dd_index[date])]

                if get_values_by_date == "T": #Rainfall less than 0.1 mm
                    get_values_by_date = 0.09

                else:
                    try:
                        get_values_by_date = int(get_values_by_date)
                    except:
                        get_values_by_date = float(get_values_by_date)

                check_day = calendar.monthrange(int(split_year_month[-1]), int(split_year_month[0] ))[1]

                if int(dd_values[date]) <= check_day:

                    if np.isnan(get_values_by_date):
                        values_packed = {
                            self.output_filename: -99.0
                            }
                    else:
                        values_packed = {
                            self.output_filename: get_values_by_date
                            }

                    temp_dict_in[date_arranged] = values_packed
            try:
                dict_dataframe[str(tmd_station)].update(temp_dict_in)
            except:
                dict_dataframe[str(tmd_station)] = temp_dict_in

        return dict_dataframe


    def updateDictionary(self, oldOne, newOne):
        for staion_id, value in newOne.items():
            for date, target in value.items():

                if staion_id not in oldOne.keys():
                    oldOne[staion_id] = newOne[staion_id]
                else:
                    try:
                        oldOne[staion_id][date].update(newOne[staion_id][date])
                    except KeyError:
                        value_merged = {**oldOne[staion_id], **newOne[staion_id]}
                        oldOne[staion_id] = value_merged

        return oldOne

    def save_json_file(self, filename):
        output_file = filename + ".json"
        with open(output_file, 'w') as outfile:
            json.dump(self.dict_dataframe, outfile)

    def main(self, html_file_path, output_file_name):
        self.html_file_path = html_file_path
        self.output_filename = output_file_name
        self.station_id = self.get_station_id(self.csv_file_name)
        
        head_tb, body_tb = self.read_html(self.html_file_path)
        data_received = self.reformat_every_day(head_tb, body_tb)

        self.dict_dataframe  = self.updateDictionary(self.dict_dataframe, data_received)

        return self.dict_dataframe
        

if __name__ == '__main__':
    import glob
    # parser = argparse.ArgumentParser()
    # parser.add_argument("html_file_path", help="html file path", type=str)
    # parser.add_argument("csv_file_name", help="csv file path", type=str)
    # parser.add_argument("output_file_name", help="output file name", type=str)

    # args = parser.parse_args()

    # tmd_class = tmd_html2json(args.html_file_path, args.csv_file_name, args.output_file_name)
    # tmd_class = tmd_html2json("Soil_temperature_at0cm_every3hrs.htm", 'TMD-Station-Metadata.csv', 'relative_humidity')

    tmd_class = tmd_html2json('TMD-Station-Metadata.csv')
    all_files = glob.glob(r"C:\Users\thipawan\Desktop\workspaces\TMD\Eng_TMD\*.htm") #Eng_TMD, tmd2

    for file_ in all_files:
        file_split_name = file_.split("\\")[-1].split("-")[0][6:]
        print(file_)
        tmd_class.main(file_, file_split_name)

    tmd_class.save_json_file('TMD_weather_data_converted_tmd3')
        # break
